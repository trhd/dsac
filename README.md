DATA STRUCTURES AND ALGORITHMS FOR C
====================================

ABOUT
-----

As the title suggests, this is a (small) collection of data structures and
algorithms implemented in and for C.


STATUS
------

This project has (for now at least) been moved to [Sourcehut](https://sr.ht/~trhd/dsac).
